﻿using Persistencia.app.dao.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.app.dao
{
    public interface TipoEmpleadoDAO
    {
        bool crear(TipoEmpleado empleado);

        bool actualizar(TipoEmpleado empleado);

        bool borrar(TipoEmpleado empleado);

        List<TipoEmpleado> listarTodo();

    }
}
