﻿using Persistencia.app.dao.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.app.dao
{
    public interface EmpleadoDAO
    {

        // Se declaran los metodos

        bool crear(Empleado empleado);

        bool actualizar(Empleado empleado);

        bool borrar(Empleado empleado);

        List<Empleado> listarTodo();


    }
}
