﻿using Persistencia.app.dao.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.app.dao
{
    public interface UsuarioDAO
    {
        bool crear(Usuario empleado);

        bool actualizar(Usuario empleado);

        bool borrar(Usuario empleado);

        List<Usuario> listarTodo();

        Usuario ValidarUSuario(string usuario, string contrasena);

    }
}
