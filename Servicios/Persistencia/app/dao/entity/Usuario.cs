﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.app.dao.entity
{
    public class Usuario
    {
        private string username;
        private string password;
        private string nombres;
        private string apellidos;
        private string Perfil;

        public Usuario()
        {
        }

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Nombres { get => nombres; set => nombres = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public string Perfil1 { get => Perfil; set => Perfil = value; }
    }
}
