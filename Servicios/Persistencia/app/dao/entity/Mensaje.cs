﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.app.dao.entity
{
    public class Mensaje
    {
        private bool transaccionOk;
        private string mensajeError;

        public Mensaje()
        {

        }

        public bool TransaccionOk { get => transaccionOk; set => transaccionOk = value; }
        public string MensajeError { get => mensajeError; set => mensajeError = value; }
    }
}
