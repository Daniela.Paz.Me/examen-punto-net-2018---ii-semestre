﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.app.dao.entity
{
    public class MensajeValidacionUsuario
    {
        private Mensaje mensaje;
        private Usuario usuario;

        public MensajeValidacionUsuario()
        {

        }

        public Mensaje Mensaje { get => mensaje; set => mensaje = value; }
        public Usuario Usuario { get => usuario; set => usuario = value; }
    }
}
