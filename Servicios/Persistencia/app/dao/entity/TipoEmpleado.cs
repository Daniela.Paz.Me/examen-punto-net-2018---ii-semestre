﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.app.dao.entity
{
    public class TipoEmpleado
    {
        private int codigo;
        private string nombre;

        public TipoEmpleado()
        {

        }

        public int Codigo { get => codigo; set => codigo = value; }
        public string Nombre { get => nombre; set => nombre = value; }
    }
}
