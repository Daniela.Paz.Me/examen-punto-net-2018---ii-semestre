﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.app.dao.entity
{
    public class Empleado
    {
        private string rut;
        private string nombres;
        private string apellido_pat;
        private string apellido_mat;
        private TipoEmpleado tipo_empleado;
        private int telefono;
        private int remuneracion;
        private DateTime fecha_nac;

        public Empleado()
        {

        }

        public string Rut { get => rut; set => rut = value; }
        public string Nombres { get => nombres; set => nombres = value; }
        public string Apellido_pat { get => apellido_pat; set => apellido_pat = value; }
        public string Apellido_mat { get => apellido_mat; set => apellido_mat = value; }
        public TipoEmpleado Tipo_empleado { get => tipo_empleado; set => tipo_empleado = value; }
        public int Telefono { get => telefono; set => telefono = value; }
        public int Remuneracion { get => remuneracion; set => remuneracion = value; }
        public DateTime Fecha_nac { get => fecha_nac; set => fecha_nac = value; }
    }
}
