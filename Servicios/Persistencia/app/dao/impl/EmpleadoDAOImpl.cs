﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Persistencia.app.dao.entity;
using Persistencia.PRUEBA3DataSetTableAdapters;
using static Persistencia.PRUEBA3DataSet;

namespace Persistencia.app.dao.impl
{
    public class EmpleadoDAOImpl : EmpleadoDAO
    {
        // Se implementa el dao

        // Se declara adaptador de BD
        private EMPLEADOTableAdapter adaptador;

        // Constructor instanciar el adaptador de la bd
        public EmpleadoDAOImpl()
        {
            adaptador = new EMPLEADOTableAdapter();
        }

        // Aca se declaran las consultas a la BD
        public bool actualizar(Empleado empleado)
        {
            try
            {
                // Se llama a la consulta "Actualizar" y se le dan los parametros
                adaptador.ActualizarEmpleado(empleado.Nombres, empleado.Apellido_pat,
                    empleado.Apellido_mat, empleado.Tipo_empleado.Codigo, empleado.Telefono,
                    empleado.Remuneracion, empleado.Fecha_nac, empleado.Rut);
                // Retornara verdadero si la consulta se ejecuta de forma correcta
                return true;
            }
            catch (Exception ex)
            {
                // retornara false si la consulta no se ejecuta o genera error
                return false;
            }

        }

        public bool borrar(Empleado empleado)
        {
            try
            {
                // Se llama a la consulta "Actualizar" y se le dan los parametros
                adaptador.BorrarEmpleado(empleado.Rut);
                // Retornara verdadero si la consulta se ejecuta de forma correcta
                return true;
            }
            catch (Exception ex)
            {
                // retornara false si la consulta no se ejecuta o genera error
                return false;
            }
        }

        public bool crear(Empleado empleado)
        {
            try
            {
                // Se llama a la consulta "Crear" y se le dan los parametros
                adaptador.CrearEmpleado(empleado.Rut, empleado.Nombres, empleado.Apellido_pat,
                    empleado.Apellido_mat, empleado.Tipo_empleado.Codigo, empleado.Telefono,
                    empleado.Remuneracion, empleado.Fecha_nac);
                // Retornara verdadero si la consulta se ejecuta de forma correcta
                return true;
            }
            catch (Exception ex)
            {
                // retornara false si la consulta no se ejecuta o genera error
                return false;
            }
        }

        public List<Empleado> listarTodo()
        {
            List<Empleado> empleados = new List<Empleado>();
            TIPO_EMPLEADOTableAdapter tipoempleadoadapter = new TIPO_EMPLEADOTableAdapter();

            // se recorre las filas de la bd
            // La consulta "GetData" viene por defecto en el dataset
            foreach (EMPLEADORow row in adaptador.GetData().Rows)
            {
                // Se crea objeto de clase Empleado
                Empleado empleado = new Empleado();
                

                // Se crea objeto de clase Tipo Empleado
                // Para castear el tipo de dato
                empleado.Tipo_empleado = new TipoEmpleado();

                // Se carga el objeto creado
                empleado.Rut = row.RUN_DV;
                empleado.Nombres = row.NOMBRES;
                empleado.Apellido_pat = row.APELLIDO_PAT;
                empleado.Apellido_mat = row.APELLIDO_MAT;
                empleado.Tipo_empleado.Codigo = row.TIPO_EMPLEADO_ID;

                foreach (TIPO_EMPLEADORow rowT in tipoempleadoadapter.BuscarPorCodigo(row.TIPO_EMPLEADO_ID)) {
                    empleado.Tipo_empleado.Nombre = rowT.NOMBRE;

                }

                empleado.Telefono =  (int)row.TELEFONO;
                empleado.Remuneracion = (int)row.REMUNERACION_BRUTA;
                empleado.Fecha_nac = row.FECHA_NACIMIENTO;

                // se carga el objeto a la lista
                empleados.Add(empleado);
            }

            return empleados;
        }
    }
}
