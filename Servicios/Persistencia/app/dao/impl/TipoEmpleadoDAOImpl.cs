﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Persistencia.app.dao.entity;
using static Persistencia.PRUEBA3DataSet;
using Persistencia.PRUEBA3DataSetTableAdapters;

namespace Persistencia.app.dao.impl
{
    // Se implementan los metodos del DAO
    public class TipoEmpleadoDAOImpl : TipoEmpleadoDAO
    {
        // Se declara adaptador de BD
        private TIPO_EMPLEADOTableAdapter adaptador;

        public TipoEmpleadoDAOImpl()
        {
            adaptador = new TIPO_EMPLEADOTableAdapter();
        }
        
        public bool actualizar(TipoEmpleado empleado)
        {
            return true;
        }

        public bool borrar(TipoEmpleado empleado)
        {
            return true;
        }

        public bool crear(TipoEmpleado empleado)
        {
            return true;
        }

        public List<TipoEmpleado> listarTodo()
        {

            List<TipoEmpleado> tipoEmpleados = new List<TipoEmpleado>();

            // se recorre las filas de la bd
            // La consulta "GetData" viene por defecto en el dataset
            foreach (TIPO_EMPLEADORow row in adaptador.GetData().Rows)
            {
                // Se crea objeto de clase TipoEmpleado
                TipoEmpleado tipoEmpleado = new TipoEmpleado();

                // Se carga el objeto creado
                tipoEmpleado.Codigo = row.CODIGO;
                tipoEmpleado.Nombre = row.NOMBRE;

                // se carga el objeto a la lista
                tipoEmpleados.Add(tipoEmpleado);
            }

            return tipoEmpleados;


        }
    }
}
