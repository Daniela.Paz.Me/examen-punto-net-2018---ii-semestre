﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Persistencia.app.dao.entity;
using Persistencia.PRUEBA3DataSetTableAdapters;
using static Persistencia.PRUEBA3DataSet;

namespace Persistencia.app.dao.impl
{
    public class UsuarioDAOImpl : UsuarioDAO
    {
        // Se declara adaptador de BD
        private USUARIOTableAdapter adaptador;


        public UsuarioDAOImpl()
        {
            adaptador = new USUARIOTableAdapter();
        }

        public bool actualizar(Usuario empleado)
        {
            return true;
        }

        public bool borrar(Usuario empleado)
        {
            return true;
        }

        public bool crear(Usuario empleado)
        {
            return true;
        }

        public List<Usuario> listarTodo()
        {

            List<Usuario> usuarios = new List<Usuario>();

            // se recorre las filas de la bd
            // La consulta "GetData" viene por defecto en el dataset
            foreach (USUARIORow row in adaptador.GetData().Rows)
            {
                // Se crea objeto de clase Usuario
                Usuario usuario = new Usuario();

                // Se carga el objeto creado
                usuario.Nombres = row.NOMBRES;
                usuario.Apellidos = row.APELLIDOS;
                usuario.Username = row.USERNAME;
                usuario.Perfil1 = row.PERFIL;
                usuario.Password = row.PASSWORD;

                // se carga el objeto a la lista
                usuarios.Add(usuario);
            }

            return usuarios;
        }

        public Usuario ValidarUSuario(string usuario, string contrasena)
        {
            Usuario usuarion = null;
            try
            {
              
                    USUARIODataTable consulta = adaptador.BuscarUsuario(usuario, contrasena);

                    if (consulta != null)
                    {
                        foreach (USUARIORow row in consulta.Rows)
                        {
                            usuarion = new Usuario();
                            usuarion.Username = row.USERNAME;
                            usuarion.Password = row.PASSWORD;
                            usuarion.Nombres = row.NOMBRES;
                            usuarion.Apellidos = row.APELLIDOS;
                            usuarion.Perfil1 = row.PERFIL;
                        }
                    }

            }
            catch (Exception ex)
            {

            }
            return usuarion;
        }
    }
}
