﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Persistencia.app.dao.entity;
using Negocio.app.reglas.genericas;

namespace ServiciosWeb
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código y en el archivo de configuración a la vez.
    public class Service1 : IService1
    {
        private EmpleadoNegocio eNeg;
        private TipoEmpleadoNegocio teNeg;
        private UsuarioNegocio uNeg;

        public Service1()
        {
            eNeg = new EmpleadoNegocio();
            teNeg = new TipoEmpleadoNegocio();
            uNeg = new UsuarioNegocio();
        }


        // Empleado
        public bool ActualizarEmpleado(Empleado empleado)
        {
            return eNeg.actualizar(empleado);
        }

        public bool BorrarEmpleado(Empleado empleado)
        {
            return eNeg.borrar(empleado);
        }

        public Mensaje CrearEmpleado(Empleado empleado)
        {
            return eNeg.crear(empleado);
        }

        public List<Empleado> ListarTodosLosEmpleados()
        {
            return eNeg.listarTodo();
        }

        // Tipo Empleado

        public bool ActualizarTipoEmpleado(TipoEmpleado tipoEmpleado)
        {
            return teNeg.actualizar(tipoEmpleado);
        }

        public bool BorrarTipoEmpleado(TipoEmpleado tipoEmpleado)
        {
            return teNeg.borrar(tipoEmpleado);
        }

        public bool CrearTipoEmpleado(TipoEmpleado tipoEmpleado)
        {
            return teNeg.crear(tipoEmpleado);
        }

        public List<TipoEmpleado> ListarTodosLosTipoEmpleado()
        {
            return teNeg.listarTodo();
        }


        // Usuario

        public bool ActualizarUsuario(Usuario usuario)
        {
            return uNeg.actualizar(usuario);
        }
        

        public bool BorrarUsuario(Usuario usuario)
        {
            return uNeg.borrar(usuario);
        }

        public bool CrearUsuario(Usuario usuario)
        {
            return uNeg.crear(usuario);
        }
        
        public List<Usuario> ListarTodosLosUsuario()
        {
            return uNeg.listarTodo();
        }

        public MensajeValidacionUsuario validacion(string usuario, string contrasena)
        {
            return uNeg.validacion(usuario, contrasena);
        }

    }
}
