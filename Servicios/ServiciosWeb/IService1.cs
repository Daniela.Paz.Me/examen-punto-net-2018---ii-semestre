﻿using Persistencia.app.dao.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiciosWeb
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {
        // Empleado
        [OperationContract]
        Mensaje CrearEmpleado(Empleado empleado);

        [OperationContract]
        bool ActualizarEmpleado(Empleado empleado);

        [OperationContract]
        bool BorrarEmpleado(Empleado empleado);

        [OperationContract]
        List<Empleado> ListarTodosLosEmpleados();

        // Usuario
        [OperationContract]
        bool CrearUsuario(Usuario usuario);

        [OperationContract]
        bool ActualizarUsuario(Usuario usuario);

        [OperationContract]
        bool BorrarUsuario(Usuario usuario);

        [OperationContract]
        List<Usuario> ListarTodosLosUsuario();


        [OperationContract]
        MensajeValidacionUsuario validacion(string usuario, string contrasena);


        // Tipo Empleado
        [OperationContract]
        bool CrearTipoEmpleado(TipoEmpleado tipoEmpleado);

        [OperationContract]
        bool ActualizarTipoEmpleado(TipoEmpleado tipoEmpleado);

        [OperationContract]
        bool BorrarTipoEmpleado(TipoEmpleado tipoEmpleado);

        [OperationContract]
        List<TipoEmpleado> ListarTodosLosTipoEmpleado();

    }
    
}
