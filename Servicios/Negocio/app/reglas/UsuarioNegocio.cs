﻿using Persistencia.app.dao;
using Persistencia.app.dao.entity;
using Persistencia.app.dao.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.app.reglas.genericas
{
    public class UsuarioNegocio
    {
        private UsuarioDAO dao;

        public UsuarioNegocio()
        {
            dao = new UsuarioDAOImpl();
        }

        public bool crear(Usuario usuario)
        {

            return dao.crear(usuario);
        }

        public bool actualizar(Usuario usuario)
        {
            return dao.actualizar(usuario);
        }

        public bool borrar(Usuario usuario)
        {
            return dao.borrar(usuario);
        }

        public List<Usuario> listarTodo()
        {
            return dao.listarTodo();
        }

        public MensajeValidacionUsuario validacion(string usuario,
            string contrasena)
        {
            MensajeValidacionUsuario mensajeValida = new MensajeValidacionUsuario();
            Mensaje mensaje = new Mensaje();
            Usuario usuarion = dao.ValidarUSuario(usuario, contrasena);

            if (usuarion!=null)
            {
                mensaje.TransaccionOk = true;
                mensaje.MensajeError = "";
                mensajeValida.Mensaje = mensaje;
                mensajeValida.Usuario = usuarion;
                
            }
            else
            {
                mensaje.TransaccionOk = false;
                mensaje.MensajeError = "Usuario o contrasena incorrecta";
                mensajeValida.Usuario = usuarion;
                mensajeValida.Mensaje = mensaje;
            }

            return mensajeValida;
        }
        
    }
}
