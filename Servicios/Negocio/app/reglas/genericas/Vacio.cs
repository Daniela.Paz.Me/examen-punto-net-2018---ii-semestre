﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.app.reglas.genericas
{
    public class Vacio
    {
        public void esVacio(string valorAtributo, string nombreAtributo)
        {
            if (!(valorAtributo != null
                && valorAtributo.Trim().Length > 0))
            {
                throw new Exception(nombreAtributo + " no puede ser blanco o vacio ");
            }
        }

        public string mensajeEsVacio(string valorAtributo, string nombreAtributo)
        {
            string mensajeError = "";

            try
            {
                esVacio(valorAtributo, nombreAtributo);
            }
            catch (Exception ex)
            {
                mensajeError = ex.Message;
            }

            return mensajeError;

        }


    }
}
