﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.app.reglas.genericas
{
    public class Tamanio
    {
        public void tamanioCorrecto(string valorAtributo,
            string nombreAtributo, int tamanioMaximo)
        {
            if (!(valorAtributo.Length <= tamanioMaximo))
            {
                throw new Exception( "Revisar" + nombreAtributo + "Tiene un tamaño maximo de  " + tamanioMaximo);

            }
        }

        public string mensajeTamanioCorrecto(string valorAtributo,
            string nombreAtributo, int tamanioMaximo)
        {
            string mensajeError = "";
            try
            {
                tamanioCorrecto(valorAtributo, nombreAtributo, tamanioMaximo);
            }
            catch (Exception ex)
            {
                mensajeError = ex.Message;
            }

            return mensajeError;
        }
    }
}
