﻿using Persistencia.app.dao;
using Persistencia.app.dao.entity;
using Persistencia.app.dao.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.app.reglas.genericas
{
    public class TipoEmpleadoNegocio
    {
        private TipoEmpleadoDAO dao;

        // Falta agregar reglas genericas

        public TipoEmpleadoNegocio()
        {
            dao = new TipoEmpleadoDAOImpl();
        }

        public bool crear(TipoEmpleado tipoempleado)
        {

            return dao.crear(tipoempleado);
        }

        public bool actualizar(TipoEmpleado tipoempleado)
        {
            return dao.actualizar(tipoempleado);
        }

        public bool borrar(TipoEmpleado tipoempleado)
        {
            return dao.borrar(tipoempleado);
        }

        public List<TipoEmpleado> listarTodo()
        {
            return dao.listarTodo();
        } 

    }
}
