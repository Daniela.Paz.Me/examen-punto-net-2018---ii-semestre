﻿using Persistencia.app.dao;
using Persistencia.app.dao.entity;
using Persistencia.app.dao.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.app.reglas.genericas
{
    public class EmpleadoNegocio
    {
        private EmpleadoDAO dao;
        private Vacio reglaVacio;
        private Tamanio reglaTamanio;


        // Falta agregar reglas genericas

        public EmpleadoNegocio()
        {
            dao = new EmpleadoDAOImpl();
            reglaVacio = new Vacio();
            reglaTamanio = new Tamanio();
           
        }


        public Mensaje crear(Empleado empleado)
        {

            Mensaje mensaje = new Mensaje();
            string mensajeError = "";

            #region reglas de negocio 

            // Mensaje de error Vacio
            mensajeError = mensajeError + reglaVacio.mensajeEsVacio(empleado.Rut, "Rut");
            mensajeError = mensajeError + reglaVacio.mensajeEsVacio(empleado.Nombres, "Nombres");
            mensajeError = mensajeError + reglaVacio.mensajeEsVacio(empleado.Apellido_pat, "Apellido Paterno");
            mensajeError = mensajeError + reglaVacio.mensajeEsVacio(empleado.Apellido_mat, "Apellido Materno");

            // mensaje nulo
            if (empleado.Tipo_empleado == null)
            {
                mensajeError = mensajeError +  "Debe seleccionar un Tipo Empleado ";
            }

            // Enterno con valor 0 
            if (empleado.Telefono == 0)
            {
                mensajeError = mensajeError +  "Debe ingresar un numero telefonico";
            }


            if (empleado.Remuneracion == 0)
            {
                mensajeError = mensajeError + "Debe ingresar una Remuneración";
            }

            // Fecha Nula
            if (empleado.Fecha_nac !=null)
            {
                mensajeError = mensajeError + reglaVacio.mensajeEsVacio(empleado.Fecha_nac.ToString(), "Fecha de Nacimiento");

            }
            
            // Mensaje de error por tamaño Maximo
            mensajeError = mensajeError + reglaTamanio.mensajeTamanioCorrecto(empleado.Rut,"Rut",18);
            mensajeError = mensajeError + reglaTamanio.mensajeTamanioCorrecto(empleado.Nombres, "Nombres",60);
            mensajeError = mensajeError + reglaTamanio.mensajeTamanioCorrecto(empleado.Apellido_pat, "Apellido Paterno",60);
            mensajeError = mensajeError + reglaTamanio.mensajeTamanioCorrecto(empleado.Apellido_mat, "Apellido Materno",60);

            // Mensaje por dato ingresado
            if (empleado.Tipo_empleado == null)
            {
                mensajeError = mensajeError + "Debe seleccionar un Tipo Empleado ";
               
            }

            
            mensajeError = mensajeError + reglaTamanio.mensajeTamanioCorrecto(empleado.Telefono.ToString(), "Telefono",9);
            mensajeError = mensajeError + reglaTamanio.mensajeTamanioCorrecto(empleado.Remuneracion.ToString(), "Remuneración",9);
            mensajeError = mensajeError + reglaTamanio.mensajeTamanioCorrecto(empleado.Fecha_nac.ToString(), "Fecha de Nacimiento",25);


            #endregion

            if (mensajeError.Length == 0)
            {
                dao.crear(empleado);
                mensaje.TransaccionOk = true;
                mensaje.MensajeError = "Cliente creado";
            }
            else
            {
                mensaje.MensajeError = mensajeError;
            }
            return mensaje;
        }

        public bool actualizar(Empleado empleado)
        {
            return dao.actualizar(empleado);
        }

        public bool borrar(Empleado empleado)
        {
            return dao.borrar(empleado);
        }

        public List<Empleado> listarTodo()
        {
            return dao.listarTodo();
        }
        
    }
}
