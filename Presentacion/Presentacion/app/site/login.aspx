﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Presentacion.app.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
     <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
</head>
<body>
    <form id="frm_log" runat="server">
        <div class="container" id="log-in-form">
            <div class="heading">
            <label>Login</label>
            </div>
     
            <div class="form-group">
                <asp:label ID="lbl_usuario" runat="server" Text="Usuario" > </asp:label>
                <asp:TextBox ID="txt_usuario" runat="server" Text=" " class="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:label ID="lbl_contrasena" runat="server" Text="Contraseña" > </asp:label>
            <asp:TextBox ID="txt_contrasena" runat="server" Text=" " TextMode="Password" class="form-control"></asp:TextBox>
            </div>
            <div class="form-group form-group-btn">
                <asp:Button ID="btn_login" runat="server" Text="INGRESAR " OnClick="btn_login_Click" class="btn btn-primary" />
            </div>
            <div class="clearfix"></div>
  
        </div>

        <asp:label ID="lbl_error" runat="server" Text=" "></asp:label>
    </form>
</body>
</html>
