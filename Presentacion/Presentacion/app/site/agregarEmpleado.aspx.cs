﻿using Presentacion.ServicioPrueba3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentacion.app.site
{
    public partial class agregarEmpleado : System.Web.UI.Page
    {

        private TipoEmpleado[] empleados;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Cliente servicio
                Service1Client servicioe = new Service1Client();
                empleados = servicioe.ListarTodosLosTipoEmpleado();

                ddlTipoEmpleado.DataSource = empleados;
                ddlTipoEmpleado.DataBind();

            }

        }

        protected void btn_agregar_Click(object sender, EventArgs e)
        {
            string rut = txtRut.Text;
            string nombre = txtNombres.Text;
            string apellidop = txt_apellido_pat.Text;
            string apellidom = txt_apellido_mat.Text;
            string tipoempleado = ddlTipoEmpleado.Text;
            int telefono =  Convert.ToInt32(txtTelefono.Text);
            int remuneracion = Convert.ToInt32(txt_remuneracion.Text);
            DateTime fecha = DateTime.Parse(txt_fecha_nacimiento.Text);

            Empleado empleado = new Empleado();
            empleado.Tipo_empleado = new TipoEmpleado();
            empleado.Tipo_empleado.Codigo = Convert.ToInt32(tipoempleado);
            empleado.Rut = rut;
            empleado.Nombres = nombre;
            empleado.Apellido_pat = apellidop;
            empleado.Apellido_mat = apellidom;
            empleado.Telefono = telefono;
            empleado.Remuneracion = remuneracion;
            empleado.Fecha_nac = fecha;

            // Cliente servicio
            Service1Client servicio = new Service1Client();
            servicio.CrearEmpleado(empleado);


            Mensaje mensaje = servicio.CrearEmpleado(empleado);
            lblMensaje.Text = mensaje.MensajeError;
            

        }
    }
}