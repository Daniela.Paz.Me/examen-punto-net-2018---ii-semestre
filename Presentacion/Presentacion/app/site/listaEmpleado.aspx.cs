﻿using Presentacion.ServicioPrueba3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentacion.app.site
{
    public partial class listaEmpleado : System.Web.UI.Page
    {
        // Se instancia empleado
        private Empleado[] empleado;


        protected void Page_Load(object sender, EventArgs e)
        {
            TipoEmpleado templeado = (TipoEmpleado)Session["tipoempleado"];

            // Trae a la lista Empleado
            Service1Client cliente = new Service1Client();
            empleado = cliente.ListarTodosLosEmpleados();

            if (!IsPostBack)
            {
                gvEmpleados.DataSource = empleado;
                gvEmpleados.DataBind();
            }

        }

    
    }
}