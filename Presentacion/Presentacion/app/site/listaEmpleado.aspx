﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/site/template/template.Master" AutoEventWireup="true" CodeBehind="listaEmpleado.aspx.cs" Inherits="Presentacion.app.site.listaEmpleado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctp_head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ctp_body" runat="server">
    <h2>
        LISTAR EMPLEADO
    </h2>
    <asp:Panel ID="pnlListaEmpleados" runat="server">

        <asp:GridView ID="gvEmpleados" runat="server"
            AutoGenerateColumns="false" >
            <Columns>
                <asp:BoundField DataField="Rut"
                    HeaderText="Rut Cliente"
                    ReadOnly="true" />

                <asp:BoundField DataField="Nombres"
                    HeaderText="Nombres"
                    ReadOnly="true" />

                <asp:BoundField DataField="Apellido_pat"
                    HeaderText="Apellido Paterno"
                    ReadOnly="true" />

                <asp:BoundField DataField="Apellido_mat"
                    HeaderText="Apellido Materno"
                    ReadOnly="true" />

                <asp:BoundField DataField="Tipo_empleado.Nombre"
                    HeaderText="Tipo Empleado"
                    ReadOnly="true" />

                <asp:BoundField DataField="Telefono"
                    HeaderText="Telefono"
                    ReadOnly="true" />

                <asp:BoundField DataField="Remuneracion"
                    HeaderText="Remuneracion"
                    ReadOnly="true" />
                
                <asp:BoundField DataField="Fecha_nac"
                    HeaderText="Fecha de Nacimiento"
                    dataformatstring="{0:dd-MM-yyyy}"
                    ReadOnly="false" />

              
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
