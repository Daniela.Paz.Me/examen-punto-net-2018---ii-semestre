﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Presentacion.ServicioPrueba3;

namespace Presentacion.app
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_login_Click(object sender, EventArgs e)
        {

            Service1Client cliente = new Service1Client();
            MensajeValidacionUsuario mensaje = 
                cliente.validacion(txt_usuario.Text.Trim(), txt_contrasena.Text);

            if (mensaje.Mensaje.TransaccionOk)
            {
                Session["usuario"] = mensaje.Usuario;
                Response.Redirect("home.aspx");
            }
            else
            {
                lbl_error.Text = mensaje.Mensaje.MensajeError;
            }
        }
    }
}