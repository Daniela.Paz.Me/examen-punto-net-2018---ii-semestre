﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/site/template/template.Master" AutoEventWireup="true" CodeBehind="agregarEmpleado.aspx.cs" Inherits="Presentacion.app.site.agregarEmpleado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctp_head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ctp_body" runat="server">
    <h2>
        AGREGAR EMPLEADO
    </h2>
    <fieldset>
        <legend>Ingresa la información del empleado</legend>
        <label class="col-50">
            Rut:
        </label>
        <asp:TextBox ID="txtRut" runat="server"></asp:TextBox>
        <asp:Label ID="txtErrorRut" runat="server"></asp:Label>

        <label class="col-50">
            Nombres:
        </label>
        <asp:TextBox ID="txtNombres" runat="server"></asp:TextBox>

        <label class="col-50">
            Apellido Paterno:
        </label>
        <asp:TextBox ID="txt_apellido_pat" runat="server"></asp:TextBox>

        <label class="col-50">
            Apellido Materno:
        </label>
        <asp:TextBox ID="txt_apellido_mat" runat="server"></asp:TextBox>

        <label class="col-50">
            Tipo de Empleado:
        </label>
        <asp:DropDownList ID="ddlTipoEmpleado" runat="server" DataValueField="Codigo" DataTextField="Nombre"></asp:DropDownList>

        <label class="col-50">
            Teléfono:
        </label>
        <asp:TextBox ID="txtTelefono" runat="server"></asp:TextBox>

        <label class="col-50">
            Remuneración Bruta:
        </label>
        <asp:TextBox ID="txt_remuneracion" runat="server"></asp:TextBox>
        
        <label class="col-50">
            Fecha de Nacimiento:
        </label>
        <asp:TextBox ID="txt_fecha_nacimiento" runat="server" TextMode="Date"></asp:TextBox>
        <br/>

        <asp:Button ID="btn_agregar" runat="server" Text="Agregar" OnClick="btn_agregar_Click" />
    </fieldset>

    <asp:Panel ID="pnlMensajes" runat="server">
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
    </asp:Panel>
</asp:Content>
