﻿using Presentacion.ServicioPrueba3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentacion.app.site.template
{
    public partial class template : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Usuario usuario = (Usuario)Session["usuario"];
            lbl_dato_us.Text = "Usuario: "  + usuario.Username + " - Nombre: " +
                usuario.Nombres + " " +
                usuario.Apellidos + " ";

            if (usuario != null)
            {
                if ("A".Equals(usuario.Perfil1) ||
                    "U".Equals(usuario.Perfil1))
                {
                    menu_home.Visible = true;
                    menu_listar_empleados.Visible = true;

                    if ("A".Equals(usuario.Perfil1))
                    {
                        menu_agregar_empleados.Visible = true;
                    }
                    else
                    {
                        menu_agregar_empleados.Visible = false;
                    }
                }
            }



        }
    }
}