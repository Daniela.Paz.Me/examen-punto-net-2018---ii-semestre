﻿using Presentacion.ServicioPrueba3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentacion.app.site
{
    public partial class home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Usuario usuario = (Usuario)Session["usuario"];

            if (usuario != null)
            {
                // Si tiene el perfil A o U se  cargara el home
                if ("A".Equals(usuario.Perfil1) 
                    || "U".Equals(usuario.Perfil1))
                {
                    // Mostrara la bienvenida para el usuario
                    lbl_dato_u.Text = " Bienvenido " + usuario.Nombres 
                        + " "  + usuario.Apellidos + " - " + usuario.Perfil1;
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }
    }
}